﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bookcase2
{
    [Serializable]
   public class Bookshelf
    {
        public int sfNum;
        public string name;
        public Book booksample;
        public List<Book> bookList = new List<Book>();
        public TopWord[] interTop = new TopWord[20];
        public Bookshelf(int h)
        {
            name = "group" + h;
        }
        public TopWord[] Mix(TopWord[] shelfTop, TopWord[] bookTop)
        {
             for(int i = 0; i<bookTop.Length; i++)
            {
                for(int j = 0; j < shelfTop.Length; j++)
                {
                    if (bookTop[i].wordCount > shelfTop[j].wordCount)
                    {
                        var buf1 = shelfTop[j];
                        shelfTop[j] = bookTop[i];
                        while (j < shelfTop.Length - 1)
                        {
                            shelfTop[j + 1] = buf1;
                            buf1 = shelfTop[j + 1];
                            j++;
                        }
                        break;
                    }
                }
            }
            return shelfTop;
        }
    }
}
