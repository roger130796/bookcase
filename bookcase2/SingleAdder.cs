﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bookcase2
{
    class SingleAdder:IChagingList
    {
        public void changeList(Book booki, ListView lv)
        {
            ListViewItem lvi = new ListViewItem(new string[] { booki.name, booki.shelf_namme });
            lvi.Tag = booki;
            lv.Items.Add(lvi);
        }

        private Book bk;
        
        public SingleAdder(string bookPath)
        {
            bk = new Book(bookPath);
        }
        public void Add(Bookcase case1, ListView lv)
        {
            var sL = case1.shelfList; 
            int h = sL.Count;
            int i = 0;
            int sN = 0; //shelf number
            float cM = 0; //similary to shelf
            float[,] closeMatch = new float[h, 2];
                foreach (var shF in sL)
            {
                if (shF.bookList.Count == 0)
                {
                    shF.booksample = bk;
                    shF.bookList.Add(bk);
                    bk.bkNum = 0;
                    bk.shfNum = shF.sfNum;
                    bk.placed = true;
                    bk.shelf_namme = shF.name;
                    changeList(bk, lv);
                    break;
                }
                else
                {
                    closeMatch[i, 1] = bk.chekSimil(bk.top, shF.booksample.top);
                    if (closeMatch[i, 1] > cM)
                    {
                        cM = closeMatch[i, 1];
                        sN = i;
                    }
                    i++;
                }

            }
            if (cM != 0 && bk.placed != true)
            {
                bk.placed = true;
                sL[sN].booksample.top = sL[sN].Mix(bk.top, sL[sN].booksample.top);
                bk.shelf_namme = sL[sN].name;
                bk.bkNum = sL[sN].bookList.Count - 1;
                bk.shfNum = sL[sN].sfNum;
                changeList(bk, lv);
                sL[sN].bookList.Add(bk);

            }
            else
            if (cM == 0 && bk.placed != true)
            {
                h++;
                Bookshelf shelfy = new Bookshelf(h);
                shelfy.sfNum = h - 1;
                sL.Add(shelfy);
                shelfy.booksample = bk;
                shelfy.bookList.Add(bk);
                bk.shelf_namme = shelfy.name;
                bk.bkNum = 0;
                bk.shfNum = shelfy.sfNum;
                bk.placed = true;
                changeList(bk, lv);
            }
            case1.shelfList = sL;
        }

        }
    }

