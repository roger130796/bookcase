﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bookcase2
{
    class Sorter : IChagingList
    {
        public void changeList(Book booki, ListView lv)
        {
            ListViewItem lvi = new ListViewItem(new string[] { booki.name, booki.shelf_namme });
            lvi.Tag = booki;
            lv.Items.Add(lvi);
        }

        public void sortBooks(string[] bookstorage, ListView lv, List<Bookshelf> shelfList)
        {
            int h = 1;
            foreach (var item in bookstorage)
            {
                int i = 0;
                int sN = 0; //shelf number
                float cM = 0; //similary to shelf
                float[,] closeMatch = new float[h, 2];
                Book booki = new Book(item);
                if (String.IsNullOrWhiteSpace(booki.textdata) || String.IsNullOrEmpty(booki.textdata))
                    continue;
                foreach (var shelfi in shelfList)
                {
                    if (shelfi.bookList.Count == 0)
                    {
                        shelfi.booksample = booki;
                        shelfi.bookList.Add(booki);
                        booki.bkNum = 0;
                        booki.shfNum = shelfi.sfNum;
                        booki.placed = true;
                        booki.shelf_namme = shelfi.name;
                        changeList(booki, lv);
                        break;
                    }
                    else
                    {
                        closeMatch[i, 1] = booki.chekSimil(booki.top, shelfi.booksample.top);
                        if (closeMatch[i, 1] > cM)
                        {
                            cM = closeMatch[i, 1];
                            sN = i;
                        }
                        i++;
                    }

                }
                if (cM != 0 && booki.placed != true)
                {
                    booki.placed = true;
                    shelfList[sN].booksample.top = shelfList[sN].Mix(booki.top, shelfList[sN].booksample.top);
                    booki.shelf_namme = shelfList[sN].name;
                    booki.bkNum = shelfList[sN].bookList.Count - 1;
                    booki.shfNum = shelfList[sN].sfNum;
                    changeList(booki, lv);
                    shelfList[sN].bookList.Add(booki);

                }
                else
                if (cM == 0 && booki.placed != true)
                {
                    h++;
                    Bookshelf shelfy = new Bookshelf(h);
                    shelfy.sfNum = h - 1;
                    shelfList.Add(shelfy);
                    shelfy.booksample = booki;
                    shelfy.bookList.Add(booki);
                    booki.shelf_namme = shelfy.name;
                    booki.bkNum = 0;
                    booki.shfNum = shelfy.sfNum;
                    booki.placed = true;
                    changeList(booki, lv);
                }
            }
        }
    }
}
