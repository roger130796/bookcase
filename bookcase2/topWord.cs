﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO;

namespace bookcase2
{
    [Serializable]
    public class TopWord 
    {
        public string shelfName;
        public string bookPath;
        public string word;
        public int wordCount;
        
    }
}
