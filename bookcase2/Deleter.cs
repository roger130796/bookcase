﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace bookcase2
{
    class Deleter : IChagingList
    {
       public  Bookcase case0;
        public Deleter(Bookcase case1)
        {
            case0 = case1;
        }
        public void changeList(Book bk, ListView lv) // delete (first different act)
        {
            case0.shelfList[bk.shfNum].bookList.Remove(case0.shelfList[bk.shfNum].bookList[bk.bkNum]);
            int index = lv.SelectedItems[0].Index;
            lv.Items[index].Remove();
        }

        public void refresh(Bookcase case1, ListView lv)
        {
            foreach(var shF in case1.shelfList)
            {
                foreach (var bk in shF.bookList)
                {
                    if (File.Exists(bk.path))
                    {
                        continue;
                    }
                    else changeList(bk, lv);
                }
            }
        }
    }
}
