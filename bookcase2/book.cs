﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace bookcase2
{
    [Serializable]
    public class Book
    {    
        public int bkNum;// number of book in book list on the shelf
        public int shfNum; // number of shelf where book plased
        public string name { get; private set; }
        public string path { get; private set; }
        public string textdata { get; private set; }
        public string[] shings { get; private set; }
        public bool placed;
        public string shelf_namme;
        public TopWord[] top = new TopWord[20];

        public Book()
        { }

        public Book(string stPath)
        {
            textdata = System.IO.File.ReadAllText(stPath, Encoding.Default);
            name = Path.GetFileName(stPath);
            shings = GetShingles(textdata, 1).ToArray();
            placed = false;
            makeTop(shings);
            path = stPath;
        }
        public float chekSimil(TopWord[] ourTop, TopWord[] sampleTops)
        {
            int sameItem = chekin(ourTop, sampleTops);
            int exeption = (ourTop.Length + sampleTops.Length - sameItem);
                if (exeption == 0)
                exeption = sameItem;
            {
                float similar = (float)sameItem / exeption;
                if (similar >= 0.09) return similar;
                else return 0;
            }            
        }
        private void makethisStringBetter(string[] array)
        {
            for(int i = 0; i<array.Length; i++)
            {
                if (String.IsNullOrWhiteSpace(array[i]) || String.IsNullOrEmpty(array[i]))
                    continue;
                var item = array[i];
                array[i] = array[i].ToLower();
                if (Char.IsPunctuation(item[item.Length-1]))
                    array[i] = item.Substring(0, item.Length - 1);
            }
            

        }
        private int chekin(TopWord[] ourTop, TopWord[] sampleTop)
        {
            var list = new List<string>();
            var list1 = new List<string>();
            for (int i = 0; i <= 19; i++)
            {
                list.Add(ourTop[i].word);
                list1.Add(sampleTop[i].word);
            }
            var list2 = new List<string>();
            foreach (var item in list)
            {
                foreach (var sampleItem in list1)
                {
                    if (sampleItem.Contains(item)||item.Contains(sampleItem))
                    {
                        list2.Add(item);
                    }

                    else continue;
                }
            }
            return list2.Count;
        }
        
        public IEnumerable<string> GetShingles(string s, int shingleLength)
        {
            var words = s.Split(' ');

            for (int i = 0; i < words.Length - shingleLength + 1; i++)
                yield return string.Join(" ", words, i, shingleLength);
            
        }
        public void makeTop(string[] shings)
        {

            makethisStringBetter(shings);
            Dictionary<string, int> repeats = new Dictionary<string, int>();
            foreach (var shing in shings.Select(t => t))
            {
                if (repeats.ContainsKey(shing))
                {
                    repeats[shing] += 1;
                }
                else
                {
                    repeats[shing] = 1;
                }

            }
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

            foreach (var item in repeats.OrderByDescending(v => v.Value))
            {
                if (item.Key.Length > 5)
                {
                    string st = item.Key;
                    list.Add(item);
                }
            }

            for (int i = 0; i <= 19; i++)
                try
                {
                    TopWord word = new TopWord();
                    word.word = list[i].Key;
                    word.wordCount = list[i].Value;
                    top[i] = word;
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    while (i <= 19)
                    {
                        top[i] = top[i - 1];
                        i++;
                    }
                    break;
                }
        }
      

    }
}
