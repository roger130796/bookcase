﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Threading;
using System.Diagnostics;

namespace bookcase2
{
    [Serializable]
    public partial class Form1 : Form
    {
       
       public string neededWord;
       string[] fullfilesPath;
       Bookcase case1 = new Bookcase();
        
        public Form1()
        {
            InitializeComponent();
        }

        public void refresh()
        {
            Deleter del = new Deleter(case1);
            del.refresh(case1, listView1);
        }

        public void run()
        {
            case1.sortBooks(fullfilesPath, listView1);
        }
        public void find()
        {
            Searcher searcher1 = new Searcher();
            searcher1.findBooks(textBox1.Text, case1, listView1);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                folderBrowserDialog1.ShowDialog();
                fullfilesPath = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.txt", SearchOption.AllDirectories);
                Task sort = new Task(run);
                sort.RunSynchronously();
                butSave.Enabled = true;
                butSearch.Enabled = true;

            }
            catch (Exception)
            {
                MessageBox.Show("Что-то идет не так");
            }
        }

        private void butSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Task search = new Task(find);
                search.RunSynchronously();
            }

            catch (Exception)
            {
                MessageBox.Show("Что-то идет не так");
            }
        }
        private void butSave_Click(object sender, EventArgs e)
        {
             saveFileDialog1.ShowDialog();
                string catalog = saveFileDialog1.FileName;
                case1.save(catalog, case1);
           
        }
        private void butLoad_Click(object sender, EventArgs e)
        { try
            {
               
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string catalog = openFileDialog1.FileName;
                    case1 = case1.load(catalog, listView1);
                    butSave.Enabled = true;
                    butSearch.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Что-то идет не так");
            }
        }
      private void listView1_DoubleClick(object sender, EventArgs e)
        {
            Book book = listView1.SelectedItems[0].Tag as Book;
            if (File.Exists(book.path))
                Process.Start(book.path);
            else
            {
                MessageBox.Show("Файла не существует. Элемент будет удален");
                Deleter delt = new Deleter(case1);
                delt.changeList(book, listView1);
                case1 = delt.case0; // sync
            }
        }

        private void btnAddOne_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string catalog = openFileDialog1.FileName;
                    SingleAdder sngadr = new SingleAdder(catalog);
                    sngadr.Add(case1, listView1);
                }
            }
            catch(Exception)
            { MessageBox.Show("Что-то идет не так"); }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Сейчас будет произведена проверка существования книг");
            try { 
            Task refr = new Task(refresh);
            refr.RunSynchronously();
        }

            catch (Exception)
            {
                MessageBox.Show("Что-то идет не так");
            }
}
    }
    }
