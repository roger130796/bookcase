﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bookcase2
{
    [Serializable]
    class Searcher
    {
        public string wantedText;
        public int wordCount;
        public List<TopWord> relevantList = new List<TopWord>();
       
        public void findBooks(string text, Bookcase case1, ListView listBox1)
        {
            if (!(String.IsNullOrWhiteSpace(text) || String.IsNullOrEmpty(text)))
            {
                bool founded = false;
               
                wantedText = text;
                string[] eachWord = wantedText.Split(' ');
                wordCount = eachWord.Length;
                listBox1.Items.Clear();
                foreach (var shelf in case1.shelfList)
                {
                    foreach (var book in shelf.bookList)
                    {
                        var wordinBook = book.textdata.Split(' ');
                        int countinBook = wordinBook.Length;
                        if (countinBook < wordCount)
                        {
                            continue;
                        }
                        else
                        {
                            TopWord relevant = new TopWord();
                            relevant.shelfName = shelf.name;
                            relevant.word = book.name;
                            relevant.bookPath = book.path;
                            relevant.wordCount = 0;
                            eachWord = book.GetShingles(book.textdata, wordCount).ToArray();
                            foreach (var word in eachWord)
                            {
                                if (word.Contains(wantedText))
                                {
                                    founded = true;
                                    relevant.wordCount++;
                                }
                               
                            }
                            if (relevant.wordCount > 0)
                                    relevantList.Add(relevant);
                        }
                    }
                }
                if (founded == false)
                    MessageBox.Show("Не найдено");
                else
                {
                    relevantList.Sort(delegate (TopWord x, TopWord y)
                    {
                        return y.wordCount.CompareTo(x.wordCount);
                    });
                    foreach (var item in relevantList)
                    {
                        Book book = new Book(item.bookPath);
                        ListViewItem lvi = new ListViewItem(new string[] { "В книге " + book.name + " встречено " + item.wordCount + " раз", item.shelfName });
                        lvi.Tag = book;
                        listBox1.Items.Add(lvi);
                    }
                }
            }
            else MessageBox.Show("Нужно выбрать, что искать");
        }
    }
}
