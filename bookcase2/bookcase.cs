﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bookcase2
{
    [Serializable]
    public class Bookcase : IChagingList
    {
        public void changeList(Book booki, ListView lv)
        {
            
            ListViewItem lvi = new ListViewItem(new string[] { booki.name, booki.shelf_namme });
            lvi.Tag = booki;
            lv.Items.Add(lvi);
        }
        public Bookshelf wantedShelf;
        public List<Bookshelf> shelfList = new List<Bookshelf>();
        BinaryFormatter formatter = new BinaryFormatter();
        public Bookcase()
        {
            Bookshelf shelf = new Bookshelf(1);
            shelf.sfNum = 0;
            shelfList.Add(shelf);
        }

        public void sortBooks(string[] bookstorage, ListView lv)
        {
            Sorter srt = new Sorter();
            srt.sortBooks(bookstorage, lv, shelfList);
        }


        public void save(string catalog, Bookcase case1)
        {
               using (FileStream fs = new FileStream(catalog, FileMode.OpenOrCreate))
            { 
                formatter.Serialize(fs, this.shelfList);

                MessageBox.Show("Объект сериализован");
            }
        }
        public Bookcase load(string catalog, ListView lv)
        {

            using (FileStream fs = new FileStream(catalog, FileMode.OpenOrCreate))
            {
                Bookcase newCase = new Bookcase();
                newCase.shelfList = (List<Bookshelf>)formatter.Deserialize(fs);
                MessageBox.Show("Объект десериализован");
                lv.Items.Clear();
                foreach (var shelf in newCase.shelfList)
                {
                    foreach (var book in shelf.bookList)
                    {
                        book.shelf_namme = shelf.name;
                        changeList(book, lv);
                    }
                }
                return newCase;
            }

        }
    }
}
   
